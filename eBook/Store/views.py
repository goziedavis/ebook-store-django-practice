from django.shortcuts import render
from .models import Ebook
# from django.shortcuts import HttpResponse


# Create your views here.
def store_view(request):
    data = {'eBooks': Ebook.objects.all()}
    return render(request, 'Store/index.html', data)
