from django.contrib import admin
from .models import Ebook

# Register your models here.
admin.site.register(Ebook)   # allows you to view the created model/table on the admin site
